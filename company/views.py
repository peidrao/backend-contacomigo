from rest_framework import viewsets, status
from rest_framework.response import Response

from company.models import Company
from company.serializers import CompanySerializer
from operations.models import Operation


class CompanyViewSet(viewsets.ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        company_list = list()
        if queryset:
            for query in queryset:
                company = dict()
                company['id'] = query.id
                company['company_name'] = query.company_name
                operations = [{'number_1': op.number_1, 'number_2': op.number_2, 'result': op.result,
                            'operators': op.operators} for op in Operation.objects.filter(company_id=query.id)]
                company['operations'] = operations
                company_list.append(company)

        return Response(company_list)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = CompanySerializer(instance)
        data = serializer.data
        data['operations'] = [{'number_1': op.number_1, 'number_2': op.number_2, 'result': op.result,
                               'operators': op.operators} for op in Operation.objects.filter(company_id=instance.id)]
        return Response(data, status=status.HTTP_200_OK)
