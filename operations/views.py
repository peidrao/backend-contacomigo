from rest_framework import viewsets, status
from rest_framework.response import Response


from operations.serializers import OperationSerializer, OperationViewSerializer
from operations.utils import calculate
from .models import Operation


class OperationViewSet(viewsets.ModelViewSet):
    queryset = Operation.objects.all()
    serializer_class = OperationSerializer

    def create(self, request, *args, **kwargs):
        serializer = OperationSerializer(data=request.data)

        if serializer.is_valid():
            result = calculate(serializer.data)
            data = {'result': result}
            data.update(serializer.data)
            Operation.objects.create(
                company_id=data['company'],
                number_1=data['number_1'],
                number_2=data['number_2'],
                result=result,
                operators=data['operators']
            )
        
            return Response(data=data, status=status.HTTP_201_CREATED)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def list(self, request):
        serializer = OperationViewSerializer(Operation.objects.all(), many=True)

        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = OperationViewSerializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)
