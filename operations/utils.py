
def calculate(data):
    if data['operators'] == '+':
        return data['number_1'] + data['number_2']
    elif data['operators'] == '-':
        return data['number_1'] - data['number_2']
    elif data['operators'] == '*':
        return data['number_1'] * data['number_2']
    else: 
        return data['number_1'] / data['number_2']