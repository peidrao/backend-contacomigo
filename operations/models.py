from django.db import models
from company.models import Company

class Operation(models.Model):
    OPERATORS = (
        ('+', 'Addition'),
        ('-', 'Subtraction'),
        ('*', 'Multiplication'),
        ('/', 'Division')
    )

    number_1 = models.IntegerField()
    number_2 = models.IntegerField()
    result = models.IntegerField(null=True)
    operators = models.CharField(max_length=15, choices=OPERATORS)
    
    company = models.ForeignKey(Company, on_delete=models.DO_NOTHING, null=False)

    def __str__(self) -> str:
        return f'{self.number_1} {self.operators} {self.number_2} = {self.result}'
