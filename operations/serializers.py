from rest_framework import serializers
from .models import Operation


class OperationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Operation
        fields = (
            'number_1',
            'number_2',
            'operators',
            'company'
        )


class OperationViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Operation
        fields = '__all__'