FROM python:3.9

ENV PYTHONUNBUFFERED 1
RUN mkdir /contacomigo-api

WORKDIR /contacomigo-api

COPY . /contacomigo-api

RUN pip install -r requirements.txt